from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from main import views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'brf.views.home', name='home'),
    # url(r'^brf/', include('brf.foo.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # For the main site
    url(r'^$', views.index, name='index'),
    url(r'^main/', include('main.urls')),
    url(r'^posts/', include('main.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
