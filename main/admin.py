# -*- coding: utf-8 -*-
from django.contrib import admin
from main.models import Post, Image, Comment, Message, UserProfile


class ImageInline(admin.TabularInline):
    model = Image
    extra = 1


class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Ämne och innehåll',       {'fields': ['topic', 'content']}),
        ('Datum', {'fields': ['pub_date'], 'classes': ['collapse']})
    ]
    inlines = [ImageInline]
    list_display = ('topic', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']


class PostComment(admin.ModelAdmin):
    fieldsets = [
        ('Ämne och innehåll',       {'fields': ['post', 'content', 'user']}),
        ('Datum', {'fields': ['pub_date'], 'classes': ['collapse']})
    ]
    list_display = ('user', 'content', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']


class PostMessage(admin.ModelAdmin):
    fieldsets = [
        ('Användare och innehåll',       {'fields': ['user', 'content']}),
        ('Datum', {'fields': ['pub_date'], 'classes': ['collapse']})
    ]
    list_display = ('user', 'content', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']


class PostUserProfile(admin.ModelAdmin):
    fieldsets = [
        ('Användare och innehåll',       {'fields': ['user', 'phone', 'address']}),
    ]
    list_display = ('user', 'phone', 'address')

admin.site.register(Post, PostAdmin)
admin.site.register(Comment, PostComment)
admin.site.register(Message, PostMessage)
admin.site.register(UserProfile, PostUserProfile)
