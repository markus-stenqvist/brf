# -*- coding: utf-8 -*-
import datetime
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render, redirect
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from main.models import Post, Comment, Image, Message, UserProfile
from django.contrib.auth.models import User
from django.http import Http404
from django.contrib.auth.hashers import make_password, is_password_usable


def index(request):
    return render_to_response(
        'index.html',
        {},
        context_instance=RequestContext(request)
    )


def latest(request):
    latest_posts = Post.objects.all().order_by('-pub_date')[:5]
    for i, post in enumerate(latest_posts):
        latest_posts[i].comments = Comment.objects.filter(post=post)
    context = {'latest_posts': latest_posts}
    return render(request, 'latest.html', context)


def post(request, post_id):
    try:
        post = Post.objects.get(pk=post_id)
        comments = Comment.objects.filter(post=post)
    except Post.DoesNotExist:
        raise Http404
    return render(request, 'post_detail.html', {'post': post, 'comments': comments})


def add_comment(request, pk):
    """Add a new comment."""
    data = request.POST

    if data:
        comment = Comment(
            post=Post.objects.get(pk=pk),
            user=request.user,
            content=data.get('content'),
            pub_date=datetime.datetime.today()
        )
        comment.save()
    return HttpResponseRedirect(reverse("post_detail", args=[pk]))


def me(request):
    """Change my information"""
    if not request.user.is_authenticated():
        return redirect(index)
    data = request.POST
    error = False
    if data:
        user = User.objects.get(pk=request.user.id)
        profile = UserProfile.objects.get(user=user)
        if data.get('first_name'):
            user.first_name = data.get('first_name')
        if data.get('last_name'):
            user.last_name = data.get('last_name')
        if data.get('email'):
            user.email = data.get('email')
        if data.get('username'):
            user.username = data.get('username')
        if data.get('password'):
            password = make_password(data.get('password'))
            if is_password_usable(password):
                user.password = password
            else:
                error = 'Ditt lösenord kunde tyvärr inte sparas.'
        if data.get('phone'):
            profile.phone = data.get('phone')
        if data.get('address'):
            profile.address = data.get('address')
        user.save()
        profile.save()
        return render(request, 'me.html', {'me': user, 'profile': profile, 'success': 'Din nya användarinformation har sparats!', 'error': error})
    return render(request, 'me.html', {'me': request.user, 'profile': request.user.get_profile()})


def contact(request):
    if not request.user.is_authenticated():
        return redirect(index)
    users = User.objects.all()
    for user in users:
        user.profile = user.get_profile()

    return render(request, 'contact.html', {'users': users, 'total_users': users.count()})


def discuss(request):
    if not request.user.is_authenticated():
        return redirect(index)
    data = request.POST
    if data:
        message = Message(
            user=request.user,
            content=data.get('content'),
            pub_date=datetime.datetime.today()
        )
        message.save()
    messages = Message.objects.all().order_by('-pub_date')[:30]
    return render(request, 'messages.html', {'messages': messages})


def login_user(request):
    state = "Var god logga in nedan.."
    username = password = ''

    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if request.user.groups.filter(name='Styrelse'):
                    request.session["is_styrelse"] = True
                if request.user.groups.filter(name="Admin"):
                    request.session["is_admin"] = True

                return redirect(latest)
            else:
                state = "Ditt konto är inte aktivt, vad god och kontakta en administratör."
        else:
            state = "Ditt lösenord och/eller användarnamn var inkorrekt."

    return render_to_response(
        'auth.html',
        {'state': state, 'username': username},
        context_instance=RequestContext(request)
    )


def logout_view(request):
    logout(request)
    return redirect(index)
