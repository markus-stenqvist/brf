from django.conf.urls import patterns, url
from main import views

urlpatterns = patterns('',
    # ex: /
    url(r'^$', views.index, name='index'),

    # ex: /main/login
    url(r'^login$', views.login_user, name='login'),
    url(r'^logout$', views.logout_view, name='logout'),

    # /posts/latest
    url(r'^latest$', views.latest, name='posts'),

    # ex: /news/5/
    url(r'^(?P<post_id>\d+)/$', views.post, name='post_detail'),
    url(r'^add_comment/(\d+)/$', views.add_comment, name="add_comment"),

    #ex: /main/me/
    url(r'^me$', views.me, name='me'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^discuss$', views.discuss, name='discuss'),
)