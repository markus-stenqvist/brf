# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
import datetime
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class Post(models.Model):
    topic = models.CharField(max_length=140)
    content = models.TextField()
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=3)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Nyligen publicerad?'


class Comment(models.Model):
    post = models.ForeignKey(Post)
    user = models.ForeignKey(User)
    content = models.TextField()
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=3)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Nyligen publicerad?'


class Image(models.Model):
    post = models.ForeignKey(Post)
    image = models.ImageField(upload_to='/images', height_field=None, width_field=None, max_length=500)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    phone = models.TextField()
    address = models.TextField()

    def __str__(self):
        return "%s's profile" % self.user

    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            profile, created = UserProfile.objects.get_or_create(user=instance)

    def is_styrelse(self):
        self.user.groups.filter(name='Styrelse')

    def is_admin(self):
        self.user.groups.filter(name='Admin')

    post_save.connect(create_user_profile, sender=User)


class Message(models.Model):
    user = models.ForeignKey(User)
    content = models.TextField()
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=2)